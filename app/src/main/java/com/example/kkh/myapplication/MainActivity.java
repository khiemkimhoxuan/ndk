package com.example.kkh.myapplication;

import android.app.Activity;
import android.os.Handler;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class MainActivity extends Activity {
    String message;

    @InjectView(R.id.first_res)
    TextView mTextView;
    @InjectView(R.id.second_res)
    TextView mTextView2;
    @InjectView(R.id.fibc)
    TextView mTextView3;
    @InjectView(R.id.fibjava)
    TextView mTextView4;
    @InjectView(R.id.thread_start_button)
    Button mStartNativeThreadButton;

    public static MainActivity instance;


    /**
     * A native method that is implemented by the
     * ‘hello-jni’ native library, which is packaged
     * with this application.
     */
    public native String stringFromJNI();

    public native void runQueen(int N);

    public native String printResult();

    public native int passIntCToJava();

    public native int fibonacci(int n);

    public native void startNativeThread();

    /** Load the native library where the native method
     * is stored.
     */
    static {
        System.loadLibrary("main");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);


        long startTime = System.currentTimeMillis();
        runQueen(200000);
        long endTime = System.currentTimeMillis() - startTime;
        // free();
        String resultTime = String.format("%.5f", endTime / 100000.0);
        mTextView.setText("Queen Game C result: " + resultTime);

        startTime = System.currentTimeMillis();
        enumerate(10);
        endTime = System.currentTimeMillis() - startTime;
        // free();

        resultTime = String.format("%.5f", endTime / 100000.0);
        mTextView2.setText("Queen Game Java Result: " + resultTime);


        System.out.println(passIntCToJava());
        handleFibonacci(mTextView3, mTextView4);
        instance = this;


    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
    }


    @OnClick(R.id.thread_start_button)
    public void startThreadButton(View v) {
        if(v.getId() == R.id.thread_start_button) {
            startNativeThread();
        }
    }

    public void setMsg(final String msg) {
        instance.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(instance, msg, Toast.LENGTH_LONG).show();
            }
        });
}


    private void handleFibonacci(final TextView textView3, final TextView textView4) {

        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                final long startTime = System.currentTimeMillis();
                fibonacci(40);
                final long endTime = System.currentTimeMillis() - startTime;

                final String resultTime = String.format("%.5f", endTime / 100000.0);
                textView3.setText("Fibonacci C Result: " + resultTime);

                final long startTime2 = System.currentTimeMillis();
                fibonacciJavaRecursion(40);
                final long endTime2 = System.currentTimeMillis() - startTime2;
                final String resultTime2 = String.format("%.5f", endTime2 / 100000.0);
                textView4.setText("Fibonacci Java Result: " + resultTime2);
            }
        });

    }


    private int fibonacciJavaRecursion(int n) {
        if (n <= 0) return 0;
        if (n == 1) return 1;
        return fibonacciJavaRecursion(n - 1) + fibonacciJavaRecursion(n - 2);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public static boolean isConsistent(int[] q, int n) {
        for (int i = 0; i < n; i++) {
            if (q[i] == q[n]) return false;   // same column
            if ((q[i] - q[n]) == (n - i)) return false;   // same major diagonal
            if ((q[n] - q[i]) == (n - i)) return false;   // same minor diagonal
        }
        return true;
    }

    public static void printQueens(int[] q) {
        int N = q.length;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (q[i] == j) System.out.print("Q ");
                else System.out.print("* ");
            }
            System.out.println();
        }
        System.out.println();
    }


    public static void enumerate(int N) {
        int[] a = new int[N];
        enumerate(a, 0);
    }

    public static void enumerate(int[] q, int n) {
        int N = q.length;
        if (n == N) printQueens(q);
        else {
            for (int i = 0; i < N; i++) {
                q[n] = i;
                if (isConsistent(q, n)) enumerate(q, n + 1);
            }
        }
    }


}
