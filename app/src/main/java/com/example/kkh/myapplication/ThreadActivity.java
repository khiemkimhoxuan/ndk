package com.example.kkh.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by kkh on 28.12.2014.
 */
public class ThreadActivity extends Activity {

    private EditText threadsEdit;
    private EditText iterationsEdit;
    private Button startButton;
    private TextView logView;

    static {
        System.loadLibrary("main");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thread_layout);

        //Initialize native code
        nativeInit();

        threadsEdit = (EditText) findViewById(R.id.threads_edit);
        iterationsEdit = (EditText) findViewById(R.id.iterations_edit);
        startButton = (Button) findViewById(R.id.start_button);
        logView = (TextView) findViewById(R.id.log_view);

        startButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int threads = getNumber(threadsEdit, 0);
                int iterations = getNumber(iterationsEdit, 0);

                if (threads > 0 && iterations > 0) {
                    startThreads(threads, iterations);
                }

            }
        });

    }

    @Override
    protected void onDestroy() {
        nativeFree();
        super.onDestroy();
    }

    /**
     * On native message callback.
     *
     * @param message native message.
     */
    private void onNativeMessage(final String message) {
        runOnUiThread(new Runnable() {
            public void run() {
                logView.append(message);
                logView.append("\n");
            }
        });
    }

    private static int getNumber(EditText editText, int defaultValue) {
        int value;

        try {
            value = Integer.parseInt(editText.getText().toString());
        } catch (NumberFormatException e) {
            value = defaultValue;
        }
        return value;

    }

    private void startThreads(int threads, int iterations) {
        //posixThreads(threads,iterations);
        //javaThreads(threads, iterations);
    }


    private void javaThreads(int threads, final int iterations) {
        for (int i = 0; i < threads; i++) {
            final int id = i;

            Thread thread = new Thread() {
                public void run() {
                    nativeWorker(id, iterations);
                }
            };

        }
    }


    private native void nativeInit();

    private native void nativeFree();

    private native void nativeWorker(int id, int iterations);


}

