LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := main
LOCAL_C_INCLUDES		:= $(LOCAL_PATH)
#LOCAL_SRC_FILES := main.c queengame.c threadexample.c
LOCAL_SRC_FILES := brickbreaker.cpp
LOCAL_LDLIBS := -llog -landroid -lEGL -lGLESv2


#Pure C/C++ code only
LOCAL_STATIC_LIBRARIES := android_native_app_glue

include $(BUILD_SHARED_LIBRARY)

#Pure C/C++ code only, call this to force the module in our project
$(call import-module,android/native_app_glue)
