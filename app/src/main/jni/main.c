#include <jni.h>
#include <string.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <EGL/egl.h>
#include <android/log.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

struct HumanNoid {
   char *name;
   int age;
};



int fib_recursion(int n) {
    if(n <= 0) return 0;
    if(n == 1) return 1;
    return fib_recursion(n-1) + fib_recursion(n-2);
}

int fib_iteration(int n) {
    int previous = -1;
    int result = 1;
    int i = 0;
    int sum = 0;
    for(i = 0; i <= n; i++) {
        sum = result + previous;
        previous = result;
        result = sum;
    }
    return result;
}

static char *deathjam = NULL;


JNIEXPORT jstring JNICALL Java_com_example_kkh_myapplication_MainActivity_stringFromJNI
(JNIEnv * env, jobject jObj)
{
    deathjam = malloc(200);
    deathjam = "This is hell";
    __android_log_print(ANDROID_LOG_INFO,"TAG","Message me\n");
    return (*env)->NewStringUTF(env, "Hello COLIIDE World dghfhfh!");
}

JNIEXPORT jint JNICALL Java_com_example_kkh_myapplication_MainActivity_fibonacci(JNIEnv *env, jclass obj,
                                                                                    jint n)
{
    return fib_recursion(n);
}

JNIEXPORT jobject JNICALL Java_com_example_kkh_myapplication_MainActivity_getHumanNoid(JNIEnv *env, jobject obj) {
    struct HumanNoid *android_human = malloc(sizeof(*android_human)+50*sizeof(char));
    android_human->name = malloc(50);
    android_human->name = "fsfwerwarejlj�lj�jl�";


    return android_human;
}

JNIEXPORT void JNICALL Java_com_example_kkh_myapplication_MainActivity_free(JNIEnv *env, jobject obj) {
    free(deathjam);
    deathjam = NULL;
}

JNIEXPORT jint JNICALL Java_com_example_kkh_myapplication_MainActivity_passIntCToJava(JNIEnv *env, jobject obj) {
    int i = 5660;
    return i;
}

JNIEXPORT void JNICALL Java_com_example_kkh_myapplication_MainActivity_createInstance(JNIEnv *env, jobject obj) {
    jclass mainObject = (*env)->GetObjectClass(env,obj);
    jfieldID fidMessage = (*env)->GetFieldID(env, mainObject, "message", "Ljava/lang/String;");
    jstring message = (*env)->NewStringUTF(env,"This is the end Java");
    (*env)->SetObjectField(env,mainObject,fidMessage,message);
}


