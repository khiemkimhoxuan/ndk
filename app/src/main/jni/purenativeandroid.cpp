#include <android/sensor.h>
#include <android/log.h>
#include <android_native_app_glue.h>
#include <jni.h>

#include <iostream>

using namespace std;


/**
 * This is the main entry point of a native application that is using
 * android_native_app_glue.  It runs in its own thread, with its own
 * event loop for receiving input events and doing other things.
 */
void android_main(struct android_app* state) {
    app_dummy();
    cout << "PURE NATIVE C++" << endl;
}