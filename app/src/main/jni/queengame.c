#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <android/log.h>
#include <jni.h>
#define TRUE 1
#define FALSE 0

char *print_buffer = NULL;

static int isConsistent(int q[], int n) {
    int i;
    for(i = 0; i < n; i++) {
        if(q[i] ==  q[n]) return FALSE;
        if(q[i] - q[n] == (n - i)) return FALSE;
        if(q[n] - q[i] == (n-i)) return FALSE;
    }
    return TRUE;
}

static void print_result(int board[]) {

    int n = sizeof(board);
    print_buffer = (char*) malloc(n);
    memset(print_buffer,0,sizeof(print_buffer));
    int i,j;
    for(i = 0; i < n; i++) {
        for(j = 0; j < n; j++) {
            if(board[i] == j)  {
            strcat(print_buffer,"Q ");

            }
            else {
            strcat(print_buffer,"* ");

            }
        }
        strcat(print_buffer,"\n");
    }
    strcat(print_buffer,"\n");
    }

static void enumerateRec(int board[], int n) {
    int board_length = sizeof(board);
    if(n == board_length)  {
       // print_result(board);


    }
    else {
        int i = 0;
        for(i = 0; i < board_length; i++) {
            board[n] = i;
            if(isConsistent(board,n) == TRUE) enumerateRec(board,n+1);
        }
    }
}


static void enumerate(int N) {
    int board[N];
    memset(board,0,sizeof(board));
    enumerateRec(board, 0);
}

JNIEXPORT void JNICALL  Java_com_example_kkh_myapplication_MainActivity_runQueen(JNIEnv *env, jobject obj,
jint arg) {
    int n = (int)arg;
    enumerate(n);
}

JNIEXPORT jstring JNICALL Java_com_example_kkh_myapplication_MainActivity_printResult(JNIEnv *env) {
    return (*env)->NewStringUTF(env,print_buffer);
}

