#include <jni.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <android/log.h>

#define LOGI(msg) __android_log_print(ANDROID_LOG_INFO,"THREAD_NATIVE",msg)


JavaVM* jvm1 = NULL;
jobject obj1 = NULL;
jmethodID mid1 = NULL;

jint JNI_OnLoad (JavaVM* vm, void* reserved)
{
	jvm1 = vm;
	return JNI_VERSION_1_6;
}


void *run_task(void *args) { //Threads need to perform
	JNIEnv* env = NULL;

    int n = (*jvm1)->AttachCurrentThread(jvm1,&env, NULL); //To get from JVM to JNIEnv
    if (n == 0) {

    	jstring msg = (*env)->NewStringUTF(env,"Yes Thread Running.");

        (*env)->CallVoidMethod(env, obj1, mid1, msg); //Methods the callback JAVA layer in the Callback class
        (*env)->DeleteGlobalRef(env,obj1); //Delete the reference
    	(*jvm1)->DetachCurrentThread(jvm1); //This must call, or an error, to cancel the thread is associated with the JVM
    }
    LOGI("44");
}


void init_instance(JNIEnv *env) {
	 LOGI("instance.");
    	jclass jz1 = (*env)->FindClass(env,"com/example/kkh/myapplication/MainActivity");
	 jmethodID mid = (*env)->GetMethodID(env,jz1,"<init>","()V");
	 jobject myobj = (*env)->NewObject(env,jz1,mid);
	          obj1 = (*env)->NewGlobalRef(env,myobj);
	 LOGI("OK Instance Done.");
	 mid1 = (*env)->GetMethodID(env, jz1, "setMsg", "(Ljava/lang/String;)V");
}


JNIEXPORT void JNICALL Java_com_example_kkh_myapplication_MainActivity_startNativeThread
  (JNIEnv *env) {
  init_instance(env);
    pthread_t thread1;

    int n = pthread_create(&thread1,NULL,run_task,NULL);//Start a thread, call the run_task method

    if (n != 0) {

       //Thread creation failed
    	jclass exceptionClazz = (*env)->FindClass(env,
    						"java/lang/RuntimeException");
        (*env)->ThrowNew(env,exceptionClazz, "create thread error.");

    }


}
