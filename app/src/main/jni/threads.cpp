#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

#include "threadsheader.h"

struct NativeWorkerArgs
{
    jint id;
    jint iterations;
};

static jmethodID gOnNativeMessage = NULL;

static JavaVM *gVM = NULL;
//global reference to the object
static jobject gObj = NULL;
//JNI_VERSION_1_6
void Java_com_example_kkh_myapplication_ThreadActivity_nativeInit(JNIEnv* env,
jobject obj) {

    if(gObj == NULL)  {
        gObj = env->NewGlobalRef(obj);
    }

    if(gObj == NULL) {
        return;
    }


	// If method ID is not cached
	if (NULL == gOnNativeMessage)
	{
		// Get the class from the object
		jclass clazz = env->GetObjectClass(obj);

		// Get the method id for the callback
		gOnNativeMessage = env->GetMethodID(clazz,
				"onNativeMessage",
				"(Ljava/lang/String;)V");

		// If method could not be found
		if (NULL == gOnNativeMessage)
		{
			// Get the exception class
			jclass exceptionClazz = env->FindClass(
					"java/lang/RuntimeException");

			// Throw exception
			env->ThrowNew(exceptionClazz, "Unable to find method");
		}
	}

}

void Java_com_example_kkh_myapplication_ThreadActivity_nativeFree(
        JNIEnv* env,
        jobject obj) {

}

void Java_com_example_kkh_myapplication_ThreadActivity_nativeWorker(
        JNIEnv* env,
        jobject obj,
        jint id,
        jint iterations) {

    for(jint i = 0; i < iterations; i++) {
        char message[26];
        sprintf(message,"Worker %d: Iteration %d", id, i);
        //Message from the C string
        jstring messageString = env->NewStringUTF(message);

        //Call the on native message method
        env->CallVoidMethod(obj, gOnNativeMessage, messageString);
        if(NULL != env->ExceptionOccurred()) {
        break;}
    }
    sleep(1);

}
