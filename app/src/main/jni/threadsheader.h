#include <jni.h>

#ifndef THREADSHEADER_H
#define THREADSHEADER_H
#ifdef __cplusplus
extern "C" {
#endif
    JNIEXPORT void JNICALL Java_com_example_kkh_myapplication_ThreadActivity_nativeInit
        (JNIEnv *, jobject);

    JNIEXPORT void JNICALL Java_com_example_kkh_myapplication_ThreadActivity_nativeFree
        (JNIEnv *, jobject);

    JNIEXPORT void JNICALL Java_com_example_kkh_myapplication_ThreadActivity_nativeWorker
        (JNIEnv *, jobject, jint, jint);

    JNIEXPORT void JNICALL Java_com_example_kkh_myapplication_ThreadActivity_posixThreads
        (JNIEnv *, jobject, jint, jint);
#ifdef __cplusplus
}
#endif
#endif